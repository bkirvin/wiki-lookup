const wordCount = {
  methods: {
    getWordCount () {
      console.log('selectedCode', this.selectedCode.langCode)
      const payload = {
        name: this.person,
        code: this.selectedCode.langCode
      }
      this.wikiGetWordCount(payload)
        .then(response => {
          console.log('response', response.data)
          const count = response.data.query.search[0] ? response.data.query.search[0].wordcount : 'not found'
          this.setWordCount(count)
          console.log('wordCount', this.wordCount)
        })
    }
  }
}

export default wordCount
