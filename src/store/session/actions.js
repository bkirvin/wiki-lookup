export function setPerson ({ commit }, payload) {
  commit('SET_PERSON', payload)
}
export function setAvailableLangs ({ commit }, payload) {
  commit('SET_AVAILABLE_LANGS', payload)
}
export function setSearchText ({ commit }, payload) {
  commit('SET_SEARCH_TEXT', payload)
}
export function setWordCount ({ commit }, payload) {
  commit('SET_WORD_COUNT', payload)
}
export function addLangs ({ commit }, payload) {
  commit('ADD_LANGS', payload)
}
export function setSelectedLang ({ commit }, payload) {
  commit('SET_SELECTED_LANG', payload)
}
