import { ref } from 'vue'

export default function () {
  return {
    availableLangs: [],
    wordCount: 0,
    searchText: '',
    person: '',
    selectedLang: ref('English')
  }
}
