export function SET_PERSON (state, v) {
  state.person = v
}
export function SET_AVAILABLE_LANGS (state, a) {
  state.availableLangs = a
}
export function SET_SEARCH_TEXT (state, v) {
  state.searchText = v
}
export function SET_WORD_COUNT (state, v) {
  console.log('setting word count to ', v)
  state.wordCount = v
}
export function ADD_LANGS (state, o) {
  state.availableLangs.push(o)
}
export function SET_SELECTED_LANG (state, v) {
  state.selectedLang = v
}
