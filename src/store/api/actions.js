import axios from 'axios'

export function wikiGetPerson ({ commit }, payload) {
  const config = {
    url: `https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&origin=*&titles=${payload.name}`
  }
  return axios(config)
}
export function wikiGetLanguages ({ commit }, payload) {
  const config = {
    url: `https://www.wikidata.org/w/api.php?action=wbgetentities&format=json& origin=*&props=sitelinks&ids=${payload.item}`
  }
  return axios(config)
}
export function wikiGetWordCount ({ commit }, payload) {
  const config = {
    url: `https://${payload.code}.wikipedia.org/w/api.php?format=json&origin=*&action=query&list=search&srwhat=nearmatch&srlimit=1&srsearch=${payload.name}`
  }
  return axios(config)
}
