import { version } from '../../../package.json'

export default function () {
  return {
    version
  }
}
